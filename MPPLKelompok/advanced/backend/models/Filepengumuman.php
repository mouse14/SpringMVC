<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "filepengumuman".
 *
 * @property integer $id
 * @property integer $id_pengumuman
 * @property string $nama
 * @property integer $size
 */
class Filepengumuman extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    
    public static function tableName()
    {
        return 'filepengumuman';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengumuman', 'nama', 'size'], 'required'],
            [['id_pengumuman', 'size'], 'integer'],
            [['nama'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pengumuman' => 'Id Pengumuman',
            'nama' => 'Nama',
            'size' => 'Size',
        ];
    }
}
