<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pengumuman".
 *
 * @property integer $no
 * @property string $judul
 * @property string $ditujukan_untuk
 * @property string $isi_pengumuman
 * @property string $nama_keasramaan
 * @property string $tanggal
 */
class Pengumuman extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public static function tableName()
    {
        return 'pengumuman';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ditujukan_untuk', 'tanggal'], 'required'],
            [['tanggal'], 'safe'],
            [['judul'], 'string', 'max' => 50],
            [['ditujukan_untuk', 'nama_keasramaan'], 'string', 'max' => 30],
            [['isi_pengumuman'], 'string', 'max' => 9999],
            [['file'], 'file','maxFiles' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'no' => 'No',
            'judul' => 'Judul',
            'ditujukan_untuk' => 'Ditujukan Untuk',
            'isi_pengumuman' => 'Isi Pengumuman',
            'nama_keasramaan' => 'Nama Keasramaan',
            'tanggal' => 'Tanggal',
        ];
    }
}
