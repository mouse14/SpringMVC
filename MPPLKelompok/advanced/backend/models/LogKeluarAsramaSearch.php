<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\LogKeluarAsrama;

/**
 * LogKeluarAsramaSearch represents the model behind the search form about `backend\models\LogKeluarAsrama`.
 */
class LogKeluarAsramaSearch extends LogKeluarAsrama
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no'], 'integer'],
            [['nama', 'nim', 'waktu_keluar', 'waktu_kembali', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogKeluarAsrama::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'no' => $this->no,
            'waktu_keluar' => $this->waktu_keluar,
            'waktu_kembali' => $this->waktu_kembali,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'nim', $this->nim])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
