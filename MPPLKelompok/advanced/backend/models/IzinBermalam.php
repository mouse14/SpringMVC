<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "izin_bermalam".
 *
 * @property integer $no
 * @property string $nama
 * @property string $nim
 * @property string $no_telepon_orangtua
 * @property string $tujuan_ib
 * @property string $tanggal_keberangkatan
 * @property string $tanggal_kembali
 * @property string $ib_membawa_laptop
 * @property string $keperluan_ib
 * @property string $nid_keasramaan
 * @property string $status
 *
 * @property Mahasiswa $nim0
 */
class IzinBermalam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public static function tableName()
    {
        return 'izin_bermalam';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_keberangkatan', 'tanggal_kembali'], 'safe'],
            [['nama', 'no_telepon_orangtua', 'tujuan_ib', 'keperluan_ib'], 'string', 'max' => 30],
            [['nim', 'ib_membawa_laptop', 'nid_keasramaan', 'status'], 'string', 'max' => 10],
            [['nim'], 'exist', 'skipOnError' => true, 'targetClass' => Mahasiswa::className(), 'targetAttribute' => ['nim' => 'nim']],
            [['file'], 'file','maxFiles' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'no' => 'No',
            'nama' => 'Nama',
            'nim' => 'Nim',
            'no_telepon_orangtua' => 'No Telepon Orangtua',
            'tujuan_ib' => 'Tujuan Ib',
            'tanggal_keberangkatan' => 'Tanggal Keberangkatan',
            'tanggal_kembali' => 'Tanggal Kembali',
            'ib_membawa_laptop' => 'Ib Membawa Laptop',
            'keperluan_ib' => 'Keperluan Ib',
            'nid_keasramaan' => 'Keasramaan TTD',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNim0()
    {
        return $this->hasOne(Mahasiswa::className(), ['nim' => 'nim']);
    }
}
