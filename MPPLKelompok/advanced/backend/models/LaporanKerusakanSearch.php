<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Laporankerusakan;

/**
 * LaporanKerusakanSearch represents the model behind the search form about `backend\models\Laporankerusakan`.
 */
class LaporanKerusakanSearch extends Laporankerusakan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nolaporan'], 'integer'],
            [['namapelapor', 'nimpelapor', 'keterangan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Laporankerusakan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'nolaporan' => $this->nolaporan,
        ]);

        $query->andFilterWhere(['like', 'namapelapor', $this->namapelapor])
            ->andFilterWhere(['like', 'nimpelapor', $this->nimpelapor])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}
