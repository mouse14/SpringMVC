<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "logkeluarmasuk".
 *
 * @property string $id
 * @property string $nim
 * @property string $nama
 * @property string $status
 */
class Logkeluarmasuk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logkeluarmasuk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nim', 'status'], 'string', 'max' => 10],
            [['nama'], 'string', 'max' => 35],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nim' => 'Nim',
            'nama' => 'Nama',
            'status' => 'Status',
        ];
    }
}
