<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "mahasiswa".
 *
 * @property string $nim
 * @property string $nama
 * @property string $notlpon
 * @property string $alamat
 * @property integer $idnilai
 * @property string $lantai
 * @property string $kamar
 * @property string $notlponortu
 * @property string $jenisKelamin
 *
 * @property IzinBermalam[] $izinBermalams
 * @property Laporankerusakan[] $laporankerusakans
 * @property Logkeluarmasuk[] $logkeluarmasuks
 */
class Mahasiswa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mahasiswa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nim', 'jenisKelamin'], 'required'],            
            [['nim', 'lantai', 'kamar', 'jenisKelamin', 'idnilai'], 'string', 'max' => 10],
            [['nama', 'notlpon', 'alamat', 'notlponortu'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nim' => 'Nim',
            'nama' => 'Nama',
            'notlpon' => 'Notlpon',
            'alamat' => 'Alamat',
            'idnilai' => 'Nilai Keasramaan',
            'lantai' => 'Lantai',
            'kamar' => 'Kamar',
            'notlponortu' => 'Notlponortu',
            'jenisKelamin' => 'Jenis Kelamin',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIzinBermalams()
    {
        return $this->hasMany(IzinBermalam::className(), ['nim' => 'nim']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLaporankerusakans()
    {
        return $this->hasMany(Laporankerusakan::className(), ['nimpelapor' => 'nim']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogkeluarmasuks()
    {
        return $this->hasMany(Logkeluarmasuk::className(), ['nim' => 'nim']);
    }
}
