<?php
use yii\helpers\Html;
use yii\bootstrap\Carousel;
/* @var $this yii\web\View */

$this->title = '';
?>			
<div class="site-index">
    <div class="body-content">
        <div class="row">
        	<div class="col-lg-7">
                <div class="box-header with-border">
                    <h3><b>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Selamat Datang Di Sistem Informasi Asrama</b></h3>
                    <br>
                    <br>
                    <br>
                     <?php echo Carousel::widget(
                        ['items' => [
                            ['content' => '<img src="slider/del1.jpg"/>',
                           
                            'options' => ['interval' => '600']
                            ],
                            ['content' => '<img src="slider/del2.jpg"/>',                           
                            'options' => ['interval' => '600']
                            ],
                            ['content' => '<img src="slider/del4.jpg"/>',
                            'options' => ['interval' => '600']
                            ],  
                            ['content' => '<img src="slider/del3.jpg"/>',
                            'options' => ['interval' => '600']
                            ],          
                        ]]); 
                    ?>      
                </div>
                </div>
               
            <div class="col-lg-5">
                
                <div class="box box-solid bg-blue-gradient">

                    <div class="box-header">
                        <i class="fa fa-info"></i>
                        <h3 class="box-title">Pengumuman</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body no-padding">
                        <!--The calendar -->
                        
                    </div><!-- /.box-body -->
                    <div class="box-footer text-blue">
                        <?php foreach ($dataProvider as $model) { //echo '<h4>' . '<i class="fa fa-tag">' . '&copy' . Html::a(Html::encode($model->judul), ['pengumuman/view', 'id' => $model->id]) . '</i>' . '</h4>';?>
                            <div class="dashboard-pengumuman">
                                <div class="title">
                                    <strong><h4><i class="fa fa-tag">&copy<?=Html::a(Html::encode($model->judul), ['pengumuman/view', 'id' => $model->no])?> </i></h4></strong>
                                </div>
                                <div class="meta">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-group">    	<?=$model->ditujukan_untuk?></i>&nbsp;&nbsp;
                                    <i class="fa fa-clock-o"> <?=$model->tanggal?></i> 
                                </div>
                            </div>

                        <?php } ?>

                    </div>
            </div><!-- /.box -->

		</div>
	</div>
</div>
