<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\Filepengumuman;


/* @var $this yii\web\View */
/* @var $model backend\models\Pengumuman */

$this->title = $model->judul;
$this->params['breadcrumbs'][] = ['label' => 'Pengumuman', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

    
        <div class="col-md-14">
            <div class="box box-solid" >
                <div class="box-header with-border" >
                    <h3><strong><?= $model->judul ?></strong></h3>
                </div>
                <div class="box-body">
                    <p>
                        <?php                        
                            Html::a('Hapus', ['delete', 'id' => $model->no], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => 'Apakah Anda Ingin Menghapus Pengumuman?',
                                    'method' => 'post',
                                ],
                            ]);                        
                        ?>
                    </p>

                    
                    <p>Tanggal : <?= $model->tanggal?></p>
                    <?php
                        $keasramaan = backend\models\Keasramaan::find()->where(['nama'=>$model->nama_keasramaan])->one();
                        if($keasramaan != null){
                            echo "<p>Oleh  :".$keasramaan->nama."</p>";
                        }
                        else{
                            echo "<p>Oleh : Akademik</p>";
                        }
                        
                    ?>
                   
                    <?= $model->isi_pengumuman ?> 
                    
                                    
                    <?php 
                    $filess = Filepengumuman::find()->where(['id_pengumuman' => $model->no])->all();                    
                    if($filess != null){
                        echo " <table class='table'>
                        <thead>
                        <th>Nama File:</th>
                        <th>Size:</th>
                        </thead>
                        <tbody>";

                        echo     "<tr>";
                        foreach ($filess as $key => $filee){ 
                        echo     "<td>" . Html::a(Html::encode($filee->nama), ['download', 'nama' => $filee->nama])."</td>";
                        echo    "<td>".number_format($filee->size / 1024, 2) . ' KB'."</td>";                           
                        
                        echo       "</tr>";
                        
                        }
                        echo "</tbody>";                            
                        echo "</table>";                    
                    
                }
                    ?>
                    


                </div><!-- /.box-body -->
            </div><!-- /. box -->
        </div><!-- /.col -->    