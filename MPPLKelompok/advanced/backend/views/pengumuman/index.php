<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PengumumanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pengumuman';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengumuman-index">
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
        
            'judul',
            'ditujukan_untuk',        
            'tanggal',

            [        
              'class' => 'yii\grid\ActionColumn',
              'header' => 'Actions',
              'headerOptions' => ['style' => 'color:#337ab7'],
              'template' => '{view}{delete} ',
              'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'view'),
                    ]);
                },
        
                'delete' => function ($url, $model) {

                return Yii::$app->user->identity->role == 'Admin' 
                    ? Html::a('<span class="glyphicon glyphicon-remove-circle"></span>', $url, ['title' => Yii::t('app', 'delete'),
                 'data-method'=>'post']):'';
                },
                        
        
            ],

              'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'view') {
                    $url = ['view', 'id'=>$model->no];
                    return $url;
                }              
                if ($action === 'delete') {
                    $url = ['delete', 'id' =>$model->no];
                return $url;
                }
                    
            }
          ],
        ],
    ]); ?>
</div>
