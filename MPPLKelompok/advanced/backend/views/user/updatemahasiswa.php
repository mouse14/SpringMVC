<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\checkBoxList;
use backend\models\User;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Dropdown;
use yii\widgets\Menu;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */

use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;


$this->title = 'Update Mahasiswa: ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-form">
<br>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password_hash')->textInput(['maxlength' => true]) ?>    

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>    

    <?= $form->field($modelk, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelk, 'notlpon')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelk, 'alamat')->textInput(['maxlength' => true]) ?>    

    <?= $form->field($modelk, 'lantai')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelk, 'kamar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelk, 'notlponortu')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelk, 'jenisKelamin')->radioList(['L'=> 'Laki-Laki', 'P' => 'Perempuan']) ?> 
    
    <?= $form->field($model, 'image')->fileInput()?>  

    <?= $form->field($model,'role')->dropDownList(['Admin'=>'Admin', 'Mahasiswa'=> 'Mahasiswa', 'Satpam' => 'Satpam'],['prompt' => '--Select Role--'])?>


    <div class="form-group">
           <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>           
        <?= Html::a('Back', ['site/index'], ['class' => 'btn btn-primary']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>

