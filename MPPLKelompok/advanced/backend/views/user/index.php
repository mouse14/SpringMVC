<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'auth_key',
            'password_hash',
            //'password_reset_token',
            // 'email:email',
            // 'status',
            // 'created_at',
            // 'updated_at',
            // 'role',

             [        
              'class' => 'yii\grid\ActionColumn',
              'header' => 'Actions',
              'headerOptions' => ['style' => 'color:#337ab7'],
              'template' => '{view}{update}{delete} ',
              'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'view'),
                    ]);
                },
                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('app', 'update'),
                    ]);
                },
        
                'delete' => function ($url, $model) {

                return Yii::$app->user->identity->role == 'Admin' 
                    ? Html::a('<span class="glyphicon glyphicon-remove-circle"></span>', $url, ['title' => Yii::t('app', 'delete'),
                 'data-method'=>'post']):'';
                },
                        
        
            ],

              'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'view') {
                    $url = ['view', 'id'=>$model->id];
                    return $url;
                } 
                if ($action === 'update') {
                    $url = ['updatecekadmin', 'id'=>$model->id];
                    return $url;
                }             
                if ($action === 'delete') {
                    $url = ['delete', 'id' =>$model->id];
                return $url;
                }
                    
            }
          ],
        ],
    ]); ?>
</div>
