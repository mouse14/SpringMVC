<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Satpam */
/* @var $form yii\widgets\ActiveForm */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo ' <center><img  width="200" height="200" src="data:image/jpeg;base64,'.base64_encode($model->image).'"/></center>';
?>

<div class="satpam-form">

	<?php $form = ActiveForm::begin(); ?>
	<?= $form->field($modelk, 'username')->textInput(['readonly' => true]) ?>    

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>    

    <?= $form->field($modelk, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelk, 'notelepon')->textInput(['maxlength' => true]) ?>       
    
    <?= $form->field($model, 'image')->fileInput()?>  


    <div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Back', ['viewcek', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
