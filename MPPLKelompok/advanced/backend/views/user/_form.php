<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\User;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Dropdown;
use yii\widgets\Menu;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */

use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;

echo Nav::widget([
    'items' => [
        ['label' => 'Keasramaan', 'url' => ['/user/createasrama']],
        ['label' => 'Mahasiswa', 'url' => ['/user/createmahasiswa']],
        ['label' => 'Satpam', 'url' => ['/user/createsatpam']],
    ],
    'options' => ['class' => 'nav nav-tabs'],
]);



?>

<div class="user-form">
<br>
    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($modelk, 'nama')->textInput(['maxlength' => true]) ?>   

    <?= $form->field($modelk, 'no_telepon')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password_hash')->textInput(['maxlength' => true]) ?>    

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image')->fileInput()?>  
    

    <?= $form->field($model,'role')->dropDownList(['Keasramaan'=>'Keasramaan', 'Mahasiswa'=> 'Mahasiswa', 'Satpam' => 'Satpam'],['prompt' => '--Select Role--'])?>

    <div class="form-group">
        <?= Html::submitButton( 'Create' , ['class' => 'btn btn-success' ]) ?>
        <?= Html::a('Back', ['site/index'], ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

