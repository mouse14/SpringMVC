<?php

use yii\helpers\Html;
use dosamigos\ckeditor\CKEditor;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\LaporanKerusakan */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','id'=>'tambah-pengumuman']]) ?>
   
        <?=
    $form->field($model, 'keterangan')->widget(CKEditor::className(), [
        'options' => ['rows' => 5],
        'preset' => 'advanced'
    ])
    ?>

    <?=
    $form->field($model, 'file[]')->widget(FileInput::classname(), [
        'options' => ['multiple' => 'true'],
        'pluginOptions' => [        
            'showUpload' => false,
            'browseLabel' => 'Browse File'
        ],
    ])
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
