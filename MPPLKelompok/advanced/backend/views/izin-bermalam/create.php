<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\IzinBermalam */

$this->title = 'Create Izin Bermalam';
$this->params['breadcrumbs'][] = ['label' => 'Izin Bermalam', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="izin-bermalam-create">
    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
