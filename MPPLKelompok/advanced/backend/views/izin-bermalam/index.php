<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\IzinBermalamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Izin Bermalam';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="izin-bermalam-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],        
            'nama',
            //'nim',
            //'kelas',
           //'no_telepon_orangtua',
            // 'tujuan_ib',
             'tanggal_keberangkatan',
             'tanggal_kembali',
            // 'ib_membawa_laptop',
            'keperluan_ib',
            'nid_keasramaan',
             'status',

         [        
              'class' => 'yii\grid\ActionColumn',
              'header' => 'Actions',
              'headerOptions' => ['style' => 'color:#337ab7'],
              'template' => '{view} {print} {accept} {reject} {batal}',
              'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'view'),
                    ]);
                },
        
                'print' => function ($url, $model) {

                return $model->status == 'Accepted' 
                    ? Html::a('<span class="glyphicon glyphicon-print"></span>', $url, ['title' => Yii::t('app', 'print'),
                                    
                ]):'';
                },

                'accept' => function ($url, $model) {

                return $model->status == 'Request' && Yii::$app->user->identity->role == 'Keasramaan' 
                    ? Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, ['title' => Yii::t('app', 'Accept'),
                ]):'';
                },

                'reject' => function ($url, $model) {

                return $model->status == 'Request' && Yii::$app->user->identity->role == 'Keasramaan' 
                    ? Html::a('<span class=" glyphicon glyphicon-remove"></span>', $url, ['title' => Yii::t('app', 'Reject'),
                ]):'';
                },


                'batal' => function ($url, $model) {

                return $model->status == 'Request' && Yii::$app->user->identity->role == 'Mahasiswa' 
                    ? Html::a('<span class="glyphicon glyphicon-remove-circle"></span>', $url, ['title' => Yii::t('app', 'Batal'),
                ]):'';
                },

        
            ],

              'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'view') {
                    $url = ['view', 'id'=>$model->no];
                    return $url;
                }              
                if ($action === 'print') {
                    $url = ['printform', 'id' =>$model->no];
                return $url;
                }

                if ($action === 'accept') {
                    $url = ['accept', 'id'=>$model->no];
                return $url;
                }
                if ($action === 'reject') {
                    $url = ['reject', 'id'=>$model->no];
                return $url;
                }
                if ($action === 'batal') {
                    $url = ['batal', 'id' => $model->no];
                return $url;
                }
            
            }
          ],
        ],
    ]); ?>
</div>
