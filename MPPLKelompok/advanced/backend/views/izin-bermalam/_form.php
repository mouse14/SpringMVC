<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\IzinBermalam */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="izin-bermalam-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nim')->textInput(['maxlength' => true]) ?>
    

    <?= $form->field($model, 'no_telepon_orangtua')->textInput() ?>

    <?= $form->field($model, 'tujuan_ib')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tanggal_keberangkatan')->widget(
        DatePicker::className(), [
    
        'clientOptions' => [                       
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);?>


    <?= $form->field($model, 'tanggal_kembali')->widget(
    DatePicker::className(), [
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);?>



<!--     <?= $form->field($model, 'ib_membawa_laptop')->radio(['label' => 'No', 'value' => '1'])?> -->     
     
    <?= $form->field($model, 'ib_membawa_laptop')->radioList(array('Y'=>'Yes','N'=>'No')); ?>
    <?= $form->field($model, 'keperluan_ib')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Back', ['view', 'id' => $model->no], ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
