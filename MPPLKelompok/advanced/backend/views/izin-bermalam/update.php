<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\IzinBermalam */

$this->title = 'Update Izin Bermalam: ' . $model->no;
$this->params['breadcrumbs'][] = ['label' => 'Izin Bermalam', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->no, 'url' => ['view', 'id' => $model->no]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="izin-bermalam-update">
    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
