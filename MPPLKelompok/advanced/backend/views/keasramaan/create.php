<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Keasramaan */

$this->title = 'Create Keasramaan';
$this->params['breadcrumbs'][] = ['label' => 'Keasramaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="keasramaan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
