<?php

namespace backend\controllers;

use Yii;
use backend\models\Pengumuman;
use backend\models\Keasramaan;
use backend\models\PengumumanSearch;
use backend\models\Filepengumuman;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PengumumanController implements the CRUD actions for Pengumuman model.
 */
class PengumumanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pengumuman models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PengumumanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pengumuman model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pengumuman model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        
        $model = new Pengumuman();

        if ($model->load(Yii::$app->request->post())) {                      
                $date = date('Y-m-d H:i:s');                        
                if (Yii::$app->user->identity->role == 'Keasramaan'){
                    $model->nama_keasramaan = Keasramaan::findOne(Yii::$app->user->identity->username)->nama;      
                }              
                else
                {
                    {
                        $model->nama_keasramaan = 'Akademik ';
                    }
                }
                $model->tanggal = $date; 
                $model->file= UploadedFile::getInstances($model, 'file'); 

                if($model->save()){
                    
                      if ($model->file != null) {
                        foreach ($model->file as $key => $files) {                            
                            $files->saveAs(Yii::getAlias('@filePath') . '/' . $files->name);
                            $file_pengumuman = new Filepengumuman();
                            $file_pengumuman->id_pengumuman = $model->no;
                            $file_pengumuman->nama = $files->name;
                            $file_pengumuman->size = $files->size;
                            $file_pengumuman->save();
                        }
                    }
                
                }                             
                else {
                    Yii::$app->session->setFlash('error', 'Pengumuman Gagal Ditambahkan!');
                }

                return $this->redirect(['view', 'id'=> $model->no]);        
            
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionDownload($nama) {
        $path = Yii::getAlias('@filePath/') ;      
          $file = $path.  $nama;
        if (file_exists($file)) {
            Yii::$app->response->sendFile($file);
        } 
    }
    /**
     * Updates an existing Pengumuman model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->no]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pengumuman model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pengumuman model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pengumuman the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pengumuman::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
